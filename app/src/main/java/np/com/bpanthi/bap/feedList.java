package np.com.bpanthi.bap;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import com.android.volley.*;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.NetworkImageView;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import org.json.*;
import java.io.*;
import java.util.*;

public class feedList extends AppCompatActivity {
    public RequestQueue requestQueue = null;
    public ImageLoader imageLoader = null;
    public ArrayList<News> newsList = new ArrayList<>();

    private String feedsDir="feeds";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_feed_list);
        this.setupImageLoader();
        loadNews();
        showAllNews();
    }

    public void setupImageLoader(){
        this.requestQueue = Volley.newRequestQueue(getApplicationContext());
        this.imageLoader = new ImageLoader(this.requestQueue, new LruBitmapCache());
    }

    void addNews(News news){
        final LinearLayout ll = (LinearLayout) findViewById(R.id.linearLayout);
        final TextView htv = new TextView(this);
        htv.setText(news.title + "  " + Integer.toString(news.id));

        final NetworkImageView img = new NetworkImageView(this);
        img.setImageUrl(news.imageUrl, this.imageLoader);

        final TextView btv = new TextView(this);
        btv.setText(news.brief);

        final Button clearB = new Button(this);
        clearB.setText("Remove News");
        clearB.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ll.removeView(htv);
                ll.removeView(btv);
                ll.removeView(img);
                ll.removeView(clearB);
            }
        });

        ll.addView(htv);
        ll.addView(img);
        ll.addView(btv);
        ll.addView(clearB);
    }

    public String loadJSONFromAsset() {
        String json = null;
        try {
            int fileId = getResources().getIdentifier("feed", "raw",getPackageName());
            InputStream is = getResources().openRawResource(fileId);
            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();
            json = new String(buffer, "UTF-8");
            parseAndAddJSONNews(json);
        } catch (IOException ex) {
            ex.printStackTrace();
            return null;
        }
        return json;
    }

    public void parseAndAddJSONNews(String json){
        try {
            Log.w("a", json);
            JSONArray feed = new JSONArray(json);
            for (int i=0; i<feed.length(); i++){
                JSONObject newsobj = feed.getJSONObject(i);
                News news = new News();
                news.fromJSONObject(newsobj);
                newsList.add(news);
            }
        } catch (Exception e){
                e.printStackTrace();
        }
    }

    public void loadJSONFromUrl(){
        String url = "http://news.hamropathshala.com/jsonfeed.php?pass=rssFeed";
        StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                    parseAndAddJSONNews(response);
                    showAllNews();
                    saveToFile(new File(getDir(feedsDir,MODE_PRIVATE),"jsonfeed.php"), response);
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.w("lse", "Error couldn't load image");
                    }
                });
        requestQueue.add(stringRequest);
    }

    public void saveToFile(File feed, String data){
        try {
            feed.createNewFile();
            FileOutputStream out = new FileOutputStream(feed);
            byte[] buffer = data.getBytes();
            out.write(buffer);
            out.close();
        }catch (Exception e){
            e.printStackTrace();
        }
    }
    public String loadNews(){
        File fDir = getDir(feedsDir, MODE_PRIVATE);
        File[] feeds = fDir.listFiles();
        String json = null;
        // load from cache
        if (feeds.length > 0) {
            try {
                File feed = feeds[0];
                FileInputStream input = new FileInputStream(feed);
                int size = input.available();
                byte[] buffer = new byte[size];
                input.read(buffer);
                input.close();
                json = new String(buffer, "UTF-8");
                parseAndAddJSONNews(json);
                Log.w("a", "Opening file from internal storage");
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
//        else {
//            try {
//                json = loadJSONFromAsset();
//                File feed = new File(fDir, "ffeed.json");
//               // saveToFile(feed, json);
//                Log.w("a", "Opening file from resources");
//            } catch (Exception e) {
//                e.printStackTrace();
//            }
//        }
        // Always load from url (in background)
        loadJSONFromUrl();
        return json;
    }

    public void showAllNews(){
        if (newsList != null) {
            Log.w("msg", "adding news");
            Log.w("msg", "count = " + Integer.toString(newsList.size()));
            for (int i = 0; i < newsList.size(); i++) {
                addNews(newsList.get(i));
            }
        } else {
            Log.w("err", "News list empty");
        }
    }
}
