package np.com.bpanthi.bap;

import android.content.Intent;
import android.os.Debug;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        final Button computeButton = (Button) findViewById(R.id.computeButton);
        computeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.w("button", "clicked compute button");
                compute();
            }
        });
    }

    public boolean leapYear(String date){
        Integer year=0,month=0,day=0, num = 0;
        for(int i=0; i<date.length(); i++){
            if (Character.isDigit(date.charAt(i))){
                num = num*10 + Character.digit(date.charAt(i), 10);
            } else{
                if (year == 0){
                    year = num;
                } else if (month == 0) {
                    month = num;
                } else {
                    day = num;
                }
                num = 0;
            }
        }
        if (year == 0) {
            year = num;
        }

        if (year % 4 == 0 && year % 100 != 0 || year % 400 == 0){
            return Boolean.TRUE;
        } else {
            return Boolean.FALSE;
        }
    }

    public void compute(){
        EditText dateBox = (EditText) findViewById(R.id.dateBox);
        String date = dateBox.getText().toString();


        EditText resultBox = (EditText) findViewById(R.id.resultBox);
        String result;
        if (leapYear(date) == Boolean.TRUE){
            result = "Leap Year!";
        } else {
            result = "Not Leap Year!";
        }

        resultBox.setText(result, TextView.BufferType.EDITABLE);

    }

    public void openFeedListActivity(View v){
        Intent intent = new Intent(MainActivity.this, feedList.class);
        MainActivity.this.startActivity(intent);

    }
}
