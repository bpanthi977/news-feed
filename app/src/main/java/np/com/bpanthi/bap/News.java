package np.com.bpanthi.bap;
import org.json.*;


public class News {
    public int id;
    public String title;
    public String brief;
    public String full;
    public String link;
    public String date;
    public String source;
    public String imageUrl;

    public void fromJSONObject(JSONObject newsobj){
        try {
            this.id = newsobj.getInt("id");
            this.title = newsobj.getString("title");
            this.brief = newsobj.getString("brief");
            this.date = newsobj.getString("date");
            this.imageUrl = newsobj.getString("image");
            this.full = newsobj.getString("full");
            this.link = newsobj.getString("link");
            this.source = newsobj.getString("source");
        } catch (JSONException e){
            e.printStackTrace();
        }

    }
}


